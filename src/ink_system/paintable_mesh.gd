class_name PaintableMesh

var _meshTool: MeshDataTool
var _meshInstance: MeshInstance3D
var _mesh: Mesh

var _transformVertexToGlobal = true
var _faceCount: int = 0

var _worldNormals: Array[Vector3] = []
var _localFaceVertices = []
var _worldVertices = []

func _init(instance: MeshInstance3D) -> void:
  assert(instance.mesh != null, "A mesh is required for it to be paintable")
  _meshInstance = instance
  _mesh = instance.mesh

  _meshTool = MeshDataTool.new()
  _meshTool.create_from_surface(_mesh, 0);

  _faceCount = _meshTool.get_face_count()
  _worldNormals.resize(_faceCount)

  _load_mesh_data()

func world_to_uv(point, normal, transform = true):
  # Gets the uv coordinates on the mesh given a point on the mesh and normal
  # these values can be obtained from a raycast
  _transformVertexToGlobal = transform

  var face = _get_face(point, normal)
  if face.size() < 3: return null

  face = face as Array
  var bc = face[2]

  var uv1 = _meshTool.get_vertex_uv(_localFaceVertices[face[0]][0])
  var uv2 = _meshTool.get_vertex_uv(_localFaceVertices[face[0]][1])
  var uv3 = _meshTool.get_vertex_uv(_localFaceVertices[face[0]][2])

  return (uv1 * bc.x) + (uv2 * bc.y) + (uv3 * bc.z)

func _load_mesh_data():
  for idx in range(_faceCount):
    _worldNormals[idx] = _meshInstance.global_transform.basis * _meshTool.get_face_normal(idx)

    var fv1 = _meshTool.get_face_vertex(idx, 0)
    var fv2 = _meshTool.get_face_vertex(idx, 1)
    var fv3 = _meshTool.get_face_vertex(idx, 2)

    _localFaceVertices.append([fv1, fv2, fv3])

    _worldVertices.append([
      _meshInstance.global_transform.basis * _meshTool.get_vertex(fv1),
      _meshInstance.global_transform.basis * _meshTool.get_vertex(fv2),
      _meshInstance.global_transform.basis * _meshTool.get_vertex(fv3),
    ])

func _get_face(point, normal, epsilon = 0.1) -> Array:
  var matches = []
  for idx in range(_faceCount):
    var world_normal = _worldNormals[idx]

    if !_equals_with_epsilon(world_normal, _meshInstance.global_transform.basis * normal, epsilon):
      continue

    var vertices = _worldVertices[idx]
    var bc = _is_point_in_triangle(point, vertices[0], vertices[1], vertices[2])

    if bc:
      matches.push_back([idx, vertices, bc])

  if matches.size() > 1:
    var closest_match
    var smallest_distance = 99999.0
    for m in matches:
      var plane := Plane(m[1][0], m[1][1], m[1][2])
      var dist = absf(plane.distance_to(point))

      if dist < smallest_distance:
        smallest_distance = dist
        closest_match = m

    return closest_match

  if matches.size() > 0:
    return matches[0]

  return []

func _equals_with_epsilon(v1, v2, epsilon):
  if (v1.distance_to(v2) < epsilon): return true
  return false

func _is_point_in_triangle(point, v1, v2, v3):
  var bc = _cart2bary(point, v1, v2, v3)

  if (bc.x < 0 or bc.x > 1) or (bc.y < 0 or bc.y > 1) or (bc.z < 0 or bc.z > 1):
    return null

  return bc

func _cart2bary(p : Vector3, a : Vector3, b : Vector3, c: Vector3) -> Vector3:
  var v0 := b - a
  var v1 := c - a
  var v2 := p - a
  var d00 := v0.dot(v0)
  var d01 := v0.dot(v1)
  var d11 := v1.dot(v1)
  var d20 := v2.dot(v0)
  var d21 := v2.dot(v1)
  var denom := d00 * d11 - d01 * d01
  var v = (d11 * d20 - d01 * d21) / denom
  var w = (d00 * d21 - d01 * d20) / denom
  var u = 1.0 - v - w
  return Vector3(u, v, w)
