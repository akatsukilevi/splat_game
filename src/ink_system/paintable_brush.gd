class_name PaintableBrush extends Node2D

var brush_queue = [];

func queue_brush(splat_pos: Vector2, color: Color = Color.RED):
  brush_queue.push_back([splat_pos, color])
  queue_redraw()

func _draw():
  # Iterate through the list
  for entry in brush_queue:
    var splat_pos: Vector2 = entry[0];
    var splat_color: Color = entry[1];
    var texture = SplatProvider.get_random_splat_texture()
    var splat_size = SplatProvider.get_random_splat_size()
    var splat_rectangle = Rect2(splat_pos.x - splat_size / 2.0, splat_pos.y - splat_size / 2.0, splat_size, splat_size)
    draw_texture_rect(texture, splat_rectangle, false, splat_color);

  # Clear the queue
  brush_queue.clear()
