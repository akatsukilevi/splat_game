extends Node

@export var splats: Array[Texture2D] = []
@export var size_range: Vector2 = Vector2(1, 5)

var random = RandomNumberGenerator.new()

func get_random_splat_texture() -> Texture2D:
  return splats[randi() % splats.size()]

func get_random_splat_size() -> float:
  return randf_range(size_range.x, size_range.y)
