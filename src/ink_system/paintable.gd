class_name Paintable extends StaticBody3D

@onready var _splatMap: SubViewport = $splat_viewport
@onready var _splatBrush: PaintableBrush = $splat_viewport/splat_brush

var _mesh: MeshInstance3D
var _uv: PaintableMesh;

func _ready() -> void:
  assert(get_parent() is MeshInstance3D)

  _mesh = get_parent()
  _uv = PaintableMesh.new(_mesh)

  var mesh_material = _mesh.get_active_material(0)
  mesh_material.next_pass = StandardMaterial3D.new()
  mesh_material.next_pass.transparency = true
  mesh_material.next_pass.albedo_texture = _splatMap.get_texture()

func splat(world_pos: Vector3, collision_normal: Vector3) -> ViewportTexture:
  var uv_pos = _uv.world_to_uv(world_pos, collision_normal)
  if !uv_pos:
    print("Could not find UV position for: ", world_pos)
    return _splatMap.get_texture()

  _splatBrush.queue_brush(uv_pos * _splatMap.size.x, Color.WHITE)

  # Return viewport
  return _splatMap.get_texture()
