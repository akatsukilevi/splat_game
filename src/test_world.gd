extends Node3D

@export var paint_decal: PackedScene

@onready var camera := $camera
@onready var indicator := $indicator
@onready var test := $ColorRect

func _process(_d):
  if Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT):
    var target = _get_target();
    if !target: return

    var collider = target[0]
    var collision_pos = target[1]
    var collision_normal = target[2]

    if !(collider is Paintable): return

    test.texture = collider.splat(collision_pos, collision_normal)

func _get_target():
  var mouse_pos = get_viewport().get_mouse_position()
  var space = get_world_3d().direct_space_state

  var ray_origin = camera.project_ray_origin(mouse_pos)
  var ray_target = ray_origin + camera.project_ray_normal(mouse_pos) * 20000

  var ray_query = PhysicsRayQueryParameters3D.new()
  ray_query.from = ray_origin
  ray_query.to = ray_target

  var raycast = space.intersect_ray(ray_query)
  if raycast.is_empty():
    return null

  indicator.global_position = raycast['position']
  return [raycast['collider'], raycast['position'], raycast['normal']]
